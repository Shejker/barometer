# Impact of Atmospheric Pressure on Well-being

This is a simple Flask web application that analyzes the impact of atmospheric pressure on well-being. The application utilizes the OpenWeatherMap API to retrieve the current atmospheric pressure for a specified location and determines the negative effects on well-being based on that pressure.

## Running the Application

1. Install the required packages by running:
   ```
   pip install -r requirements.txt
   ```

2. In the `app.py` file, update the value of `"YOUR_API_KEY"` with your own OpenWeatherMap API key, which you can obtain by creating an account on their website.

3. Run the application by executing:
   ```
   python app.py
   ```

4. The application will be running locally and accessible at `http://localhost:5000`.

## Using the Application

1. Open a web browser and navigate to `http://localhost:5000`.

2. Enter the desired location in the text field and click the "Get" button.

3. The application will display the current atmospheric pressure for the specified location along with the negative effects on well-being.

## Project Structure

- `app.py` - the main Flask application file containing routing logic and request handling.
- `templates` - directory containing HTML templates.
  - `index.html` - the main page of the application.
- `static` - directory containing static files.
  - `css` - directory containing CSS stylesheets.
    - `style.css` - the file responsible for styling the page.
- `README.md` - this file, providing information about the application.

## Dependencies

- Flask - a micro web framework for Python.
- requests - a Python library for making HTTP requests.