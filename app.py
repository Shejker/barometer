from flask import Flask, render_template, request
import requests

app = Flask(__name__)


def get_weather(city):
    url = "http://api.openweathermap.org/data/2.5/weather"
    params = {
        "q": city,
        "appid": "5d4ab8899d1d610d8e78cfc1e886b8ba"
    }
    response = requests.get(url, params=params)
    data = response.json()
    if response.status_code == 200:
        pressure = data["main"]["pressure"]
        return pressure
    else:
        return None


def get_effects(pressure):
    if pressure < 950:
        effects = "Silne bóle głowy, nudności, zmęczenie"
    elif pressure < 970:
        effects = "Zmniejszona koncentracja, ospałość"
    elif pressure < 990:
        effects = "Lekkie bóle głowy, zmęczenie"
    elif pressure < 1010:
        effects = "Brak istotnych skutków"
    elif pressure < 1030:
        effects = "Poprawa nastroju, zwiększona energia"
    elif pressure < 1040:
        effects = "Zwiększona nerwowość, nadpobudliwość"
    elif pressure < 1060:
        effects = "Możliwe podwyższone ciśnienie krwi"
    else:
        effects = "Możliwe problemy z oddychaniem"
    return effects


@app.errorhandler(404)
def page_not_found():
    return render_template('404.html'), 404


@app.route("/", methods=["GET", "POST"])
def index():
    if request.method == "POST":
        city = request.form["city"]
        pressure = get_weather(city)
        if pressure is not None:
            effects = get_effects(pressure)
            return render_template("index.html", pressure=pressure, effects=effects)
        else:
            error_message = "Nie odnaleziono miasta!"
            return render_template("index.html", error_message=error_message)
    return render_template("index.html")


if __name__ == "__main__":
    app.run(debug=True)
